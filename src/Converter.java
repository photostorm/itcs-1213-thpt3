/**
 * This class holds current sentence and the converted sentence which numbers under ten are converted to English text.
 * 
 * @author Justin E. Ervin
 * @version 2-27-2014
 */

public class Converter {
	// Declares all needed variables
	private StringBuilder currentSentence; // Holds the current sentence being converted
	private String convertedSentence; // Holds the converted sentence

	/**
	 * Constructor for objects of class CurrencyConverter
	 * 
	 * @param the original sentence
	 */
	public Converter(String sentence) {
		// Creating instance of StringBuild
		currentSentence = new StringBuilder(sentence);
		
		// Converting the sentence.
		convert();
	}

    /**
     * This method converts numbers under ten to English text.  
     * 
     */
	private void convert() {
		for (int index = 0; index < currentSentence.length(); index++) {
			if (currentSentence.charAt(index) == '0' || currentSentence.charAt(index) == '1' || currentSentence.charAt(index) == '2' || currentSentence.charAt(index) == '3' || currentSentence.charAt(index) == '4' || currentSentence.charAt(index) == '5' || currentSentence.charAt(index) == '6' || currentSentence.charAt(index) == '7' || currentSentence.charAt(index) == '8' || currentSentence.charAt(index) == '9') {
				// The current character is a digit
				if (index == 0) {
					// The beginning of the sentence
					if (currentSentence.charAt(index + 1) != '0' && currentSentence.charAt(index + 1) != '1' && currentSentence.charAt(index + 1) != '2' && currentSentence.charAt(index + 1) != '3' && currentSentence.charAt(index + 1) != '4' && currentSentence.charAt(index + 1) != '5' && currentSentence.charAt(index + 1) != '6' && currentSentence.charAt(index + 1) != '7' && currentSentence.charAt(index + 1) != '8' && currentSentence.charAt(index + 1) != '9') {
						// The next character is not a digit
						switch (currentSentence.charAt(index)) {
						case '0':
							// Replaces the number 0 to English text which is zero and makes the first character a uppercase character because it is the beginning of the sentence.
							currentSentence.insert(currentSentence.indexOf("0", index), "Zero");
							currentSentence.replace(currentSentence.indexOf("0", index), currentSentence.indexOf("0", index) + 1, "");
							break;
						case '1':
							// Replaces the number 1 to English text which is one and makes the first character a uppercase character because it is the beginning of the sentence.
							currentSentence.insert(currentSentence.indexOf("1", index), "One");
							currentSentence.replace(currentSentence.indexOf("1", index), currentSentence.indexOf("1", index) + 1, "");
							break;
						case '2':
							// Replaces the number 2 to English text which is two and makes the first character a uppercase character because it is the beginning of the sentence.
							currentSentence.insert(currentSentence.indexOf("2", index), "Two");
							currentSentence.replace(currentSentence.indexOf("2", index), currentSentence.indexOf("2", index) + 1, "");
							break;
						case '3':
							// Replaces the number 3 to English text which is three and makes the first character a uppercase character because it is the beginning of the sentence.
							currentSentence.insert(currentSentence.indexOf("3", index), "Three");
							currentSentence.replace(currentSentence.indexOf("3", index), currentSentence.indexOf("3", index) + 1, "");
							break;
						case '4':
							// Replaces the number 4 to English text which is four and makes the first character a uppercase character because it is the beginning of the sentence.
							currentSentence.insert(currentSentence.indexOf("4", index), "Four");
							currentSentence.replace(currentSentence.indexOf("4", index), currentSentence.indexOf("4", index) + 1, "");
							break;
						case '5':
							// Replaces the number 5 to English text which is five and makes the first character a uppercase character because it is the beginning of the sentence.
							currentSentence.insert(currentSentence.indexOf("5", index), "Five");
							currentSentence.replace(currentSentence.indexOf("5", index), currentSentence.indexOf("5", index) + 1, "");
							break;
						case '6':
							// Replaces the number 6 to English text which is six and makes the first character a uppercase character because it is the beginning of the sentence.
							currentSentence.insert(currentSentence.indexOf("6", index), "Six");
							currentSentence.replace(currentSentence.indexOf("6", index), currentSentence.indexOf("6", index) + 1, "");
							break;
						case '7':
							// Replaces the number 7 to English text which is seven and makes the first character a uppercase character because it is the beginning of the sentence.
							currentSentence.insert(currentSentence.indexOf("7", index), "Seven");
							currentSentence.replace(currentSentence.indexOf("7", index), currentSentence.indexOf("7", index) + 1, "");
							break;
						case '8':
							// Replaces the number 8 to English text which is eight and makes the first character a uppercase character because it is the beginning of the sentence.
							currentSentence.insert(currentSentence.indexOf("8", index), "Eight");
							currentSentence.replace(currentSentence.indexOf("8", index), currentSentence.indexOf("8", index) + 1, "");
							break;
						case '9':
							// Replaces the number 9 to English text which is nine and makes the first character a uppercase character because it is the beginning of the sentence.
							currentSentence.insert(currentSentence.indexOf("9", index), "Nine");
							currentSentence.replace(currentSentence.indexOf("9", index), currentSentence.indexOf("9", index) + 1, "");
							break;
						}
					}
				} else if (index == (currentSentence.length() - 1)) {
					// The ending of the sentence
					if (currentSentence.charAt(index - 1) != '0' && currentSentence.charAt(index - 1) != '1' && currentSentence.charAt(index - 1) != '2' && currentSentence.charAt(index - 1) != '3' && currentSentence.charAt(index - 1) != '4' && currentSentence.charAt(index - 1) != '5' && currentSentence.charAt(index - 1) != '6' && currentSentence.charAt(index - 1) != '7' && currentSentence.charAt(index - 1) != '8' && currentSentence.charAt(index - 1) != '9') {
						// The previous character is not a digit
						switch (currentSentence.charAt(index)) {
						case '0':
							// Replaces the number 0 to English text which is zero
							currentSentence.insert(currentSentence.indexOf("0", index), "zero");
							currentSentence.replace(currentSentence.indexOf("0", index), currentSentence.indexOf("0", index) + 1, "");
							break;
						case '1':
							// Replaces the number 1 to English text which is one
							currentSentence.insert(currentSentence.indexOf("1", index), "one");
							currentSentence.replace(currentSentence.indexOf("1", index), currentSentence.indexOf("1", index) + 1, "");
							break;
						case '2':
							// Replaces the number 2 to English text which is two
							currentSentence.insert(currentSentence.indexOf("2", index), "two");
							currentSentence.replace(currentSentence.indexOf("2", index), currentSentence.indexOf("2", index) + 1, "");
							break;
						case '3':
							// Replaces the number 3 to English text which is three
							currentSentence.insert(currentSentence.indexOf("3", index), "three");
							currentSentence.replace(currentSentence.indexOf("3", index), currentSentence.indexOf("3", index) + 1, "");
							break;
						case '4':
							// Replaces the number 4 to English text which is four
							currentSentence.insert(currentSentence.indexOf("4", index), "four");
							currentSentence.replace(currentSentence.indexOf("4", index), currentSentence.indexOf("4", index) + 1, "");
							break;
						case '5':
							// Replaces the number 5 to English text which is five
							currentSentence.insert(currentSentence.indexOf("5", index), "five");
							currentSentence.replace(currentSentence.indexOf("5", index), currentSentence.indexOf("5", index) + 1, "");
							break;
						case '6':
							// Replaces the number 6 to English text which is six
							currentSentence.insert(currentSentence.indexOf("6", index), "six");
							currentSentence.replace(currentSentence.indexOf("6", index), currentSentence.indexOf("6", index) + 1, "");
							break;
						case '7':
							// Replaces the number 7 to English text which is seven
							currentSentence.insert(currentSentence.indexOf("7", index), "seven");
							currentSentence.replace(currentSentence.indexOf("7", index), currentSentence.indexOf("7", index) + 1, "");
							break;
						case '8':
							// Replaces the number 8 to English text which is eight
							currentSentence.insert(currentSentence.indexOf("8", index), "eight");
							currentSentence.replace(currentSentence.indexOf("8", index), currentSentence.indexOf("8", index) + 1, "");
							break;
						case '9':
							// Replaces the number 9 to English text which is nine
							currentSentence.insert(currentSentence.indexOf("9", index), "nine");
							currentSentence.replace(currentSentence.indexOf("9", index), currentSentence.indexOf("9", index) + 1, "");
							break;
						}
					}
				} else {
					// The middle of the sentence
					if ((currentSentence.charAt(index - 1) != '0' && currentSentence.charAt(index - 1) != '1' && currentSentence.charAt(index - 1) != '2' && currentSentence.charAt(index - 1) != '3' && currentSentence.charAt(index - 1) != '4' && currentSentence.charAt(index - 1) != '5' && currentSentence.charAt(index - 1) != '6' && currentSentence.charAt(index - 1) != '7' && currentSentence.charAt(index - 1) != '8' && currentSentence.charAt(index - 1) != '9') && (currentSentence.charAt(index + 1) != '0' && currentSentence.charAt(index + 1) != '1' && currentSentence.charAt(index + 1) != '2' && currentSentence.charAt(index + 1) != '3' && currentSentence.charAt(index + 1) != '4' && currentSentence.charAt(index + 1) != '5' && currentSentence.charAt(index + 1) != '6' && currentSentence.charAt(index + 1) != '7' && currentSentence.charAt(index + 1) != '8' && currentSentence.charAt(index + 1) != '9')) {
						// The next character and previous character is not digits
						switch (currentSentence.charAt(index)) {
						case '0':
							// Replaces the number 0 to English text which is zero
							currentSentence.insert(currentSentence.indexOf("0", index), "zero");
							currentSentence.replace(currentSentence.indexOf("0", index), currentSentence.indexOf("0", index) + 1, "");
							break;
						case '1':
							// Replaces the number 1 to English text which is one
							currentSentence.insert(currentSentence.indexOf("1", index), "one");
							currentSentence.replace(currentSentence.indexOf("1", index), currentSentence.indexOf("1", index) + 1, "");
							break;
						case '2':
							// Replaces the number 2 to English text which is two
							currentSentence.insert(currentSentence.indexOf("2", index), "two");
							currentSentence.replace(currentSentence.indexOf("2", index), currentSentence.indexOf("2", index) + 1, "");
							break;
						case '3':
							// Replaces the number 3 to English text which is three
							currentSentence.insert(currentSentence.indexOf("3", index), "three");
							currentSentence.replace(currentSentence.indexOf("3", index), currentSentence.indexOf("3", index) + 1, "");
							break;
						case '4':
							// Replaces the number 4 to English text which is four
							currentSentence.insert(currentSentence.indexOf("4", index), "four");
							currentSentence.replace(currentSentence.indexOf("4", index), currentSentence.indexOf("4", index) + 1, "");
							break;
						case '5':
							// Replaces the number 5 to English text which is five
							currentSentence.insert(currentSentence.indexOf("5", index), "five");
							currentSentence.replace(currentSentence.indexOf("5", index), currentSentence.indexOf("5", index) + 1, "");
							break;
						case '6':
							// Replaces the number 6 to English text which is six
							currentSentence.insert(currentSentence.indexOf("6", index), "six");
							currentSentence.replace(currentSentence.indexOf("6", index), currentSentence.indexOf("6", index) + 1, "");
							break;
						case '7':
							// Replaces the number 7 to English text which is seven
							currentSentence.insert(currentSentence.indexOf("7", index), "seven");
							currentSentence.replace(currentSentence.indexOf("7", index), currentSentence.indexOf("7", index) + 1, "");
							break;
						case '8':
							// Replaces the number 8 to English text which is eight
							currentSentence.insert(currentSentence.indexOf("8", index), "eight");
							currentSentence.replace(currentSentence.indexOf("8", index), currentSentence.indexOf("8", index) + 1, "");
							break;
						case '9':
							// Replaces the number 9 to English text which is nine
							currentSentence.insert(currentSentence.indexOf("9", index), "nine");
							currentSentence.replace(currentSentence.indexOf("9", index), currentSentence.indexOf("9", index) + 1, "");
							break;
						}
					}
				}
			}
		}

		// Assigning the new sentence
		convertedSentence = new String(currentSentence);
	}

    /**
     * This method changes the current sentence to the new sentence and converts numbers under ten to English text.  
     * 
	 * @param the new sentence
     */
	public void changeSentence(String sentence) {
		// Changing the current sentence to the new sentence.
		currentSentence.replace(0, currentSentence.length(), sentence);
		
		// Converting the sentence.
		convert();
	}

    /**
     * This method returns the converted sentence which is textually represents this class
     * 
	 * @return the converted sentence
     */
	public String toString() {
		return convertedSentence;
	}
}