public class Rational {
	private int numerator;
	private int denominator;

	public Rational() {

	}

	public Rational(int num, int den) {
		this.numerator = num;
		this.denominator = den;
	}

	public void subtract(Rational r1, Rational r2) {
		int f1 = getGCF(r1.denominator * r2.denominator, r1.denominator);
		int f2 = getGCF(r1.denominator * r2.denominator, r2.denominator);
		numerator = (r1.numerator * f2) - (r2.numerator * f1);
		denominator = r1.denominator * r2.denominator;
	}

	public void add(Rational r1, Rational r2) {
		int f1 = getGCF(r1.denominator * r2.denominator, r1.denominator);
		int f2 = getGCF(r1.denominator * r2.denominator, r2.denominator);
		numerator = (r1.numerator * f2) + (r2.numerator * f1);
		denominator = r1.denominator * r2.denominator;
	}

	public void divide(Rational r1, Rational r2) {
		numerator = r1.numerator * r2.denominator;
		denominator = r1.denominator * r2.numerator;
	}

	public void multiply(Rational r1, Rational r2) {
		numerator = r1.numerator * r2.numerator;
		denominator = r1.denominator * r2.denominator;
	}

	public double getDecimal() {
		double num1 = numerator;
		double num2 = denominator;

		return num1 / num2;
	}

	public String getFraction() {
		return numerator + "/" + denominator;
	}

	public void reduce() {
		int GCF = getGCF(Math.abs(numerator), Math.abs(denominator));
		numerator = numerator / GCF;
		denominator = denominator / GCF;
	}

	private int getGCF(int firstNumber, int secondNumber) {
		int rem = 0;
		int gcf = 0;
		do {
			rem = firstNumber % secondNumber;
			if (rem == 0)
				gcf = secondNumber;
			else {
				firstNumber = secondNumber;
				secondNumber = rem;
			}
		} while (rem != 0);
		return gcf;
	}
}
