/**
 * This class was written to test the Converter class.
 * 
 * @author Justin E. Ervin
 * @version 2-27-2014
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Scanner;

public class Driver {

	/**
	 * Execution of this program starts in the main( ) method
	 * 
	 * @param Java arguments
	 */
	public static void main(String[] args) throws FileNotFoundException {
		// Declares all needed variables
		Scanner keyboard = new Scanner(System.in); // A reference variable for Scanner and create instance for the keyboard
		Converter sentenceConverter = null; // A reference variable for Converter
		PrintWriter outputFile; // A reference variable for PrintWriter
		Scanner inputFile; // A reference variable for Scanner for input file
		String fileName; // Holds the file name given by the user
		String sentence; // Holds the current sentence

		// Create instance of the PrintWriter for the output file
		outputFile = new PrintWriter(new File("outSentence.txt"));
		
		// Prompt the user for the file name
		System.out.println("Enter the name of the file: ");
		fileName = keyboard.nextLine();

		// Create instance of the Scanner for the file given by the user
		inputFile = new Scanner(new File(fileName));

		while (inputFile.hasNext()) {
			// Get the next line in the file
			sentence = inputFile.nextLine();

			// Check and Convert the sentence
			if (sentenceConverter == null) {
				sentenceConverter = new Converter(sentence);
			} else {
				sentenceConverter.changeSentence(sentence);
			}

			// Display the sentence to the screen
			System.out.println(sentenceConverter);

			// Save the sentence to the output file
			outputFile.println(sentenceConverter);
		}
		
		// Close the PrintWriter class that was output information to a file
		outputFile.close();
		System.out.println("\nData written to the output file.");
		
		// Close the Scanner class that was getting input from the keyboard
		keyboard.close();

		// Close the Scanner class that was getting input from a file
		inputFile.close();
	}
}
